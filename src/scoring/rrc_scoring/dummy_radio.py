import zmq
import time
import multiprocessing
import logging

log = logging.getLogger("DummyRadio")

class DummyRadio():
  def __init__(self, in_addr="127.0.0.1", in_port=44001, out_addr="127.0.0.1", out_port=44002):
    self.in_addr = "tcp://" + in_addr + ":" + str(in_port)
    self.out_addr = "tcp://" + out_addr + ":" + str(out_port)

    self.proc = multiprocessing.Process(target=self.run_fn, args=[])
    self.proc.daemon = True

  def run_fn(self):
    self.traffic_in = zmq.Context().socket(zmq.SUB)
    self.traffic_in.connect(self.in_addr)
    self.traffic_in.subscribe("")

    self.traffic_out = zmq.Context().socket(zmq.PUB)
    self.traffic_out.bind(self.out_addr)

    log.info("Running...")
    while True:
      if self.traffic_in.poll(timeout=0):
        r = self.traffic_in.recv()
        if r is not None:
          #log.debug("Got " + str(r))
          self.traffic_out.send(r)
          #log.debug("Sent " + str(r))
          r = None
      else:
        time.sleep(0.1)

  def run(self):
    self.proc.start()

  def stop(self):
    self.proc.terminate()

