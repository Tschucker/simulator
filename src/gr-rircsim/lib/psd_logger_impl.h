/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_RIRCSIM_PSD_LOGGER_IMPL_H
#define INCLUDED_RIRCSIM_PSD_LOGGER_IMPL_H

#include <rircsim/psd_logger.h>
#include <stdio.h>
#include <fftw3.h>

namespace gr {
  namespace rircsim {

    class psd_logger_impl : public psd_logger
    {
     private:
      // Nothing to declare in this block.
      std::string m_node;
      const static int NUM_POINTS = 1024;
      FILE * m_file;
      fftwf_plan m_fft_plan;
      fftwf_complex * m_fft_in;
      fftwf_complex * m_fft_out;
      float m_mags_out[NUM_POINTS];

     public:
      psd_logger_impl(std::string node);
      ~psd_logger_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace rircsim
} // namespace gr

#endif /* INCLUDED_RIRCSIM_PSD_LOGGER_IMPL_H */

