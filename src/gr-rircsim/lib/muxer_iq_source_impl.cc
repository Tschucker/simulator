/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "muxer_iq_source_impl.h"

using namespace gr::rircsim;
using boost::asio::ip::tcp;

namespace gr {
  namespace rircsim {

    muxer_iq_source::sptr
    muxer_iq_source::make(std::string host, uint16_t port)
    {
      return gnuradio::get_initial_sptr
        (new muxer_iq_source_impl(host, port));
    }


    /*
     * The private constructor
     */
    muxer_iq_source_impl::muxer_iq_source_impl(std::string host, uint16_t port)
      : gr::sync_block("muxer_iq_source",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1, 1, sizeof(gr_complex))),
              host(host),
              port(port),
              state(NODE_SOURCE_START),
              sample_offset(0),
              m_cancel(false),
              m_write_pending(false),
              m_input_chunk(0),
              m_output_chunk(1)
    {
      m_thread = boost::thread(boost::bind(&muxer_iq_source_impl::client_loop, this));
    }

    /*
     * Our virtual destructor.
     */
    muxer_iq_source_impl::~muxer_iq_source_impl()
    {
    }

    bool muxer_iq_source_impl::stop()
    {
      m_cancel = true;
      fprintf(stderr, "muxer_iq_source_impl spinning down\n");
      fflush(stderr);
    }    

    void muxer_iq_source_impl::client_loop()
    {
      while(!m_cancel)
      {
        if(state != NODE_SOURCE_CONNECTED)
        {       
          try
          {
            boost::asio::io_service io_service;
            tcp::resolver resolver(io_service);
            tcp::resolver::query query(tcp::v4(), host, std::to_string(port));
            tcp::resolver::iterator iterator = resolver.resolve(query);

            socket = boost::shared_ptr<tcp::socket>(new tcp::socket(io_service));            
            boost::asio::connect(*socket, iterator);
          }
          catch(const std::exception& e)
          {
            // connection error; sleep 1 second before retrying
            std::cerr << e.what() << '\n';
            sleep(1);
            continue;
          }

          state = NODE_SOURCE_CONNECTED;
          
          fprintf(stderr, "[node->muxer] client connected\n");
          fflush(stderr);

          // write the initial sample offset
          size_t buffer_size = sizeof(sample_offset);
          size_t written = boost::asio::write(*socket, boost::asio::buffer(&sample_offset, buffer_size));
          if(written != buffer_size)
          {
            std::cerr << "Error sample offset to muxer. Tried to wrte " << buffer_size << " bytes, wrote " << written << "." << std::endl;
            throw;
          }

          // read loop
          while(!m_cancel)
          {
            // wait for next chunk
            boost::system::error_code error;
            while(socket->available() < sizeof(m_iq_chunks[m_input_chunk]) && !m_cancel) { usleep(1); }
            socket->read_some(boost::asio::buffer(&m_iq_chunks[m_input_chunk], sizeof(m_iq_chunks[m_input_chunk])), error);

            // handler read errors
            // - if the client closed the connection, listen for reconnect
            // - throw non-disconnect errors
            // fprintf(stderr, "source read loop\n");
            if(error == boost::asio::error::eof) { 
              fprintf(stderr, "[muxer source] error!\n");
              state = NODE_SOURCE_START; 
              break; 
            }
            else if(error) throw boost::system::system_error(error);

            // wait for the previous chunk to finish
            while(m_write_pending && !m_cancel) { usleep(1); }

            // notify work(...) that it can process this chunk & swap the buffers
            m_mutex.lock();
            m_input_chunk = !m_input_chunk;
            m_output_chunk = !m_output_chunk;
            m_write_pending = true;
            m_mutex.unlock();

            // return the next sample offset
            sample_offset += m_iq_chunks[m_output_chunk].length;
            size_t written = boost::asio::write(*socket, boost::asio::buffer(&sample_offset, sizeof(sample_offset)));
            if(written != sizeof(sample_offset))
            {
              fprintf(stderr, "error writing received sample offset to server\n");
              fflush(stderr);
              throw;
            }
          }
        }
      }
    }    

    int
    muxer_iq_source_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      while(!m_cancel && !m_write_pending)
      {
        usleep(1);
      }

      if(m_cancel) return 0;
     
      m_mutex.lock();

      gr_complex *out = (gr_complex *) output_items[0];

      memcpy(out, m_iq_chunks[m_output_chunk].data, m_iq_chunks[m_output_chunk].length * sizeof(gr_complex));
      noutput_items = m_iq_chunks[m_output_chunk].length;

      m_write_pending = false;

      m_mutex.unlock();

      return noutput_items;
    }

  } /* namespace rircsim */
} /* namespace gr */

