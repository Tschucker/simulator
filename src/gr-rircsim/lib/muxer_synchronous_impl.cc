/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <volk/volk.h>
#include "muxer_synchronous_impl.h"

namespace gr {
  namespace rircsim {

    muxer_synchronous::sptr
    muxer_synchronous::make(bool blue1_enabled,
                            bool blue2_enabled,
                            bool red1_enabled,
                            bool marshal_enabled,
                            bool host_output_enabled,
                            bool blue1_red1_shortcut_enabled)
    {
      return gnuradio::get_initial_sptr
        (new muxer_synchronous_impl(blue1_enabled,
                                    blue2_enabled,
                                    red1_enabled,
                                    marshal_enabled,
                                    host_output_enabled,
                                    blue1_red1_shortcut_enabled));
    }


    /*
     * The private constructor
     */
    muxer_synchronous_impl::muxer_synchronous_impl(bool blue1_enabled,
                                                   bool blue2_enabled,
                                                   bool red1_enabled,
                                                   bool marshal_enabled,
                                                   bool host_output_enabled,
                                                   bool blue1_red1_shortcut_enabled)
      : gr::block("muxer_synchronous",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(0, 0, 0)),
              m_next_sample_offset(0),
              m_cancel(false),
              m_initialized(false),
              m_passthrough_enabled(blue1_red1_shortcut_enabled)
    {
      if(blue1_enabled)
      {
        printf("[muxer] starting blue1 servers\n");
        init_iq_in_server(BLUE1_IN_PORT, "blue1");
        init_iq_out_server(BLUE1_OUT_PORT, "blue1");
      }

      if(blue2_enabled)
      {
        printf("[muxer] starting blue2 servers\n");
        init_iq_in_server(BLUE2_IN_PORT, "blue2");
        init_iq_out_server(BLUE2_OUT_PORT, "blue2");
      }

      if(red1_enabled)
      {
        printf("[muxer] starting red1 servers\n");
        init_iq_in_server(RED1_IN_PORT, "red1");
        init_iq_out_server(RED1_OUT_PORT, "red1");
      }

      if(marshal_enabled)
      {
        printf("[marshal] starting marshal servers\n");
        init_iq_out_server(MARSHAL_OUT_PORT, "marshal");
      }

      if(host_output_enabled)
      {
        printf("[muxer] starting host output server\n");
        init_iq_out_server(HOST_OUT_PORT, "host");
      }

      printf("[muxer] starting channel input server\n");
      init_iq_in_server(CHANNEL_IN_PORT, "channel");

      for(auto m : m_in_servers) m_servers.push_back(m);
      for(auto m : m_out_servers) m_servers.push_back(m);

      m_thread = boost::thread(boost::bind(&muxer_synchronous_impl::muxer_loop, this));
      m_thread.detach();

      m_iq_chunk.end_of_burst = 0;
      m_iq_chunk.start_of_burst = 0;
    }

    /*
     * Our virtual destructor.
     */
    muxer_synchronous_impl::~muxer_synchronous_impl()
    {

    }

    bool muxer_synchronous_impl::stop()
    {
      m_cancel = true;
      fprintf(stderr, "muxer_synchronous_impl spinning down\n");
      fflush(stderr);
    }  

    void
    muxer_synchronous_impl::forecast (int noutput_items, gr_vector_int &ninput_items_required)
    {
    }

    void muxer_synchronous_impl::iq_out_server_loop(boost::shared_ptr<server_meta> server)
    {
      tcp::acceptor acceptor(server->io_service, tcp::endpoint(tcp::v4(), server->port));
      while(!m_cancel)
      {
        server->state = WAITING_FOR_CONNECTION;
        server->socket = boost::shared_ptr<tcp::socket>(new tcp::socket(server->io_service));
        acceptor.accept(*server->socket);

        // once the client has connected, the server will start
        // in state CONNECTED_STREAMING, which expects a continuous
        // IQ stream from the client
        server->state = CONNECTED_STREAMING;

        fprintf(stderr, "[muxer] output client connected: %s\n", server->name.c_str());
        fflush(stderr);

        while(!m_initialized);

        // session loop
        while(!m_cancel)
        {
          // read the next sample offset expected by the client
          boost::system::error_code error;
          size_t nso;
          if(server->socket->available() < sizeof(nso)) continue;
          server->socket->read_some(boost::asio::buffer(&nso, sizeof(nso)), error);
          server->next_sample_offset = nso;

          // handler read errors
          // - if the client closed the connection, listen for reconnect
          // - throw non-disconnect errors
          if(error == boost::asio::error::eof) break;
          else if(error) {
            std::cerr <<  boost::system::system_error(error).what() << std::endl;
            break;
          }
        }
      }
    }

    void muxer_synchronous_impl::init_iq_out_server(uint16_t port, std::string name)
    {
      boost::shared_ptr<server_meta> server = boost::shared_ptr<server_meta>(new server_meta());
      server->port = port;
      server->name = name;
      server->state = WAITING_FOR_CONNECTION;
      server->last_sample_offset = 0;
      server->next_sample_offset = -1;
      server->data_available = true;
      server->thread = boost::thread(boost::bind(&muxer_synchronous_impl::iq_out_server_loop, this, server));
      server->thread.detach();
      m_out_servers.push_back(server);
    }

    void muxer_synchronous_impl::iq_in_server_loop(boost::shared_ptr<server_meta> server)
    {
      tcp::acceptor acceptor(server->io_service, tcp::endpoint(tcp::v4(), server->port));
      while(!m_cancel)
      {
        server->state = WAITING_FOR_CONNECTION;
        server->socket = boost::shared_ptr<tcp::socket>(new tcp::socket(server->io_service));
        server->data_available = false;
        acceptor.accept(*server->socket);

        // once the client has connected, the server will start
        // in state CONNECTED_STREAMING, which expects a continuous
        // IQ stream from the client
        server->state = CONNECTED_STREAMING;
        fprintf(stderr, "[muxer] input client connected: %s\n", server->name.c_str());

        if(server->name == "blue1") server->state = CONNECTED;

        // wait for all clients to connect
        while(!m_initialized && !m_cancel) { usleep(1); };

        // session loopto
        while(!m_cancel)
        {
          while(server->data_available && !m_cancel)
          {
            usleep(1);
          }

          // wait for next chunk
          boost::system::error_code error;
          while(server->socket->available() < sizeof(server->iq_chunk)) 
          {
            usleep(1);
          }

          // read in the next chunk
          server->mutex.lock();
          size_t read = server->socket->read_some(boost::asio::buffer(&server->iq_chunk, sizeof(server->iq_chunk)), error);

          // handler read errors
          // - if the client closed the connection, listen for reconnect
          // - throw non-disconnect errors
          if(error == boost::asio::error::eof) break;
          else if(error) {
            fprintf(stderr, "[muxer] unhandled error from - input loop %s\n", server->name.c_str());
            std::cerr << boost::system::system_error(error).what() << std::endl;
            server->mutex.unlock();
            break;
          }

          if(server->iq_chunk.end_of_burst != 0)
          {
            server->iq_chunk.end_of_burst += server->delta;
          }

          server->iq_chunk.offset += server->delta;
          server->next_sample_offset = server->iq_chunk.offset + server->iq_chunk.length - server->delta;

          // write the next expected sample offset to the client
          size_t written = boost::asio::write(*server->socket, boost::asio::buffer(&server->next_sample_offset, sizeof(server->next_sample_offset)));

          server->data_available = true;

          server->mutex.unlock();        }
      }
    }

    void muxer_synchronous_impl::init_iq_in_server(uint16_t port, std::string name)
    {
      boost::shared_ptr<server_meta> server = boost::shared_ptr<server_meta>(new server_meta());
      server->port = port;
      server->name = name;
      server->state = WAITING_FOR_CONNECTION;
      server->last_sample_offset = 0;
      server->next_sample_offset = 0;
      server->data_available = false;
      server->delta = 0;
      server->last_eob = 0;
      server->thread = boost::thread(boost::bind(&muxer_synchronous_impl::iq_in_server_loop, this, server));
      server->thread.detach();
      m_in_servers.push_back(server);
    }

    void muxer_synchronous_impl::muxer_loop()
    {
      // wait for all of the the IQ clients to connects
      fprintf(stderr, "[muxer] waiting for IQ clients to connect\n");
      while(!m_cancel)
      {
        usleep(1);
        bool ready = true;
        for(auto &m : m_servers)
        {
          if(m->state == WAITING_FOR_CONNECTION) ready = false;
        }
        if(ready) break;
      }
      fprintf(stderr, "[muxer] all IQ clients have connected\n");

      m_initialized = true;

      // main muxer loop
      while(!m_cancel)
      {
        usleep(1);

        // check for available data on bursty sources
        for(auto &m : m_in_servers)
        {
          m->mutex.lock();
          if(m->state == CONNECTED && m->data_available)
          {
            m->state = CONNECTED_STREAMING;

            #ifdef MUXER_DEBUG
            fprintf(stderr, "[muxer %s] CONNECTED -> CONNECTED_STREAMING\n", m->name.c_str());
            fprintf(stderr, "  - prevEOB: %u\n", m->last_eob);
            fprintf(stderr, "  -  offset: %u\n", m->iq_chunk.offset);
            fprintf(stderr, "  -     EOB: %u\n", m->iq_chunk.end_of_burst);
            fprintf(stderr, "  -  length: %u\n", m->iq_chunk.length);
            #endif 
            m->delta = m_next_sample_offset - m->iq_chunk.offset;

            #ifdef MUXER_DEBUG
            fprintf(stderr, "  -   delta: %u\n", m->delta);
            #endif 

            if(m->iq_chunk.end_of_burst != 0)
            {
              m->iq_chunk.end_of_burst += m->delta;
            }

            m->iq_chunk.offset += m->delta;
            
            #ifdef MUXER_DEBUG
            fprintf(stderr, "  -  offset: %u\n", m->iq_chunk.offset);
            fprintf(stderr, "  -     EOB: %u\n", m->iq_chunk.end_of_burst);
            #endif
          }
          m->mutex.unlock();
        }

        // wait until all active input clients have available input data
        while(!m_cancel)
        {
          for(auto &m : m_in_servers)
          {
            m->mutex.lock();
            if(m->state == CONNECTED_STREAMING)
            {
              m_iq_chunk.offset = m->iq_chunk.offset;
              m->mutex.unlock();
              break;
            }
            m->mutex.unlock();
          }


          bool syncd = true;
          for(auto &m : m_in_servers)
          {
            m->mutex.lock();
            if(m->state == CONNECTED_STREAMING)
            {
              if(!m->data_available || (m->iq_chunk.offset != m_iq_chunk.offset)) 
              {
                syncd = false;
              }
            }
            m->mutex.unlock();
          }
          if(!syncd) 
          {
            usleep(1);
            continue;
          }
          break;
        }

        // validate that each input client is at the same sample offset
        // fprintf(stderr, "[muxer] validating input client sample offset\n");
        // m_iq_chunk.offset = m_in_servers[0]->iq_chunk.offset;
        for(auto &m : m_in_servers)
        {
          m->mutex.lock();
          if(m->state == CONNECTED_STREAMING)
          {
            if(m->iq_chunk.offset != m_iq_chunk.offset)
            {
              fprintf(stderr, "muxer input sample offset mismatch!\n");
              #ifdef MUXER_DEBUG
              for(auto &mm : m_in_servers)
              {
                printf("%s offset: %u\n", mm->name.c_str(), mm->iq_chunk.offset);
              }
              #endif
              throw;
            }
          }
          m->mutex.unlock();
        }
      
        // determine the maximum sample offset across all clients
        size_t max_offset = 0;
        for(auto &m : m_in_servers)
        {
          if(m->state == CONNECTED_STREAMING)
          {
            if(max_offset == 0)
            {
              max_offset = m->iq_chunk.length + m->iq_chunk.offset;
            }
            max_offset = std::min(max_offset, m->iq_chunk.length + m->iq_chunk.offset);
          }
        }

        // sum the input IQ
        m_iq_chunk.length = max_offset - m_iq_chunk.offset;
        memset(m_iq_chunk.data, 0, m_iq_chunk.length * sizeof(gr_complex));
        for(auto &m : m_in_servers)
        {
          m->mutex.lock();
          if(m->state == CONNECTED_STREAMING)
          {
            volk_32fc_x2_add_32fc(m_iq_chunk.data, m_iq_chunk.data, m->iq_chunk.data, m_iq_chunk.length);

            // prepare the blue1 -> red1 passthrough chunk (if applicable)
            if(m_passthrough_enabled && m->name == "blue1") {
              memcpy(&m_passthrough_iq_chunk, &m->iq_chunk, sizeof(muxer_iq_chunk_t));
            } 

            // check if we've only consumed part of this chunk
            if(m->iq_chunk.offset + m->iq_chunk.length > max_offset)
            {
              m->iq_chunk.offset = max_offset;
              m->iq_chunk.length -= m_iq_chunk.length;
              memmove(m->iq_chunk.data, &m->iq_chunk.data[m_iq_chunk.length], m->iq_chunk.length * sizeof(gr_complex));
            }

            // flag to the server that all input IQ has been consumed
            else m->data_available = false;

            // check for end-of-burst
            if(m->iq_chunk.end_of_burst != 0 && !m->data_available)
            {
              uint64_t end_of_burst_offset = m->iq_chunk.end_of_burst;
              uint64_t end_of_iq_offset = m->iq_chunk.length + m->iq_chunk.offset;

              #ifdef MUXER_DEBUG
              fprintf(stderr, "end_of_burst = %lu\n", m->iq_chunk.end_of_burst);
              fprintf(stderr, "next_sample_offset = %lu\n", m_next_sample_offset);
              #endif

              // check if the EOB is immediately followed by more IQ
              // - in this case, we've received multiple contiguous burst-tagged 
              //   packets, with the second packet triggering streaming mode
              if(end_of_iq_offset <= end_of_burst_offset)
              {
                m->state = CONNECTED_STREAMING;

                #ifdef MUXER_DEBUG
                fprintf(stderr, "\n[muxer %s] CONNECTED_STREAMING - continuation\n\n", m->name.c_str());
                fprintf(stderr, "%u %u\n", end_of_iq_offset, end_of_burst_offset);
                #endif
              }

              // true EOB; change the server mode from CONNECTED_STREAMING
              // to CONNECTED (bursty)
              else
              {
                m->state = CONNECTED;
                m->delta = 0;
                m->last_eob = end_of_burst_offset;

                #ifdef MUXER_DEBUG
                fprintf(stderr, "\n[muxer %s] CONNECTED_STREAMING -> CONNECTED\n\n", m->name.c_str());
                fprintf(stderr, "EOB FOUND AT OFFSET %lu (%lu)\n", end_of_iq_offset, end_of_burst_offset);  
                #endif
              }
            }
          }   
          m->mutex.unlock();
        }

        // write output IQ chunk to output clients
        for(auto m : m_out_servers)
        {
          if(m->state == CONNECTED_STREAMING)
          {
            while(m->next_sample_offset < m_next_sample_offset)
            {
              usleep(1);
            }

            size_t buffer_size = sizeof(m_iq_chunk);

            // write the chunk - blue1 -> red1 passthrough
            if(m_passthrough_enabled && m->name == "red1")
            {
              m_passthrough_iq_chunk.offset = m_iq_chunk.offset;
              m_passthrough_iq_chunk.start_of_burst = m_iq_chunk.start_of_burst;
              m_passthrough_iq_chunk.end_of_burst = m_iq_chunk.end_of_burst;
              m_passthrough_iq_chunk.length = m_iq_chunk.length;
              size_t written = boost::asio::write(*m->socket, boost::asio::buffer(&m_passthrough_iq_chunk, buffer_size));
              if(written != buffer_size)
              {
                std::cerr << "Error writing IQ chunk to node. Tried to wrte " << buffer_size << " bytes, wrote " << written << "." << std::endl;
                throw;
              }
            }

            // write the chunk - mixed
            else
            {
              size_t written = boost::asio::write(*m->socket, boost::asio::buffer(&m_iq_chunk, buffer_size));
              if(written != buffer_size)
              {
                std::cerr << "Error writing IQ chunk to node. Tried to wrte " << buffer_size << " bytes, wrote " << written << "." << std::endl;
                throw;
              }
            }
          }
        }
        m_next_sample_offset = m_iq_chunk.offset + m_iq_chunk.length;
      }
    }


    int
    muxer_synchronous_impl::general_work (int noutput_items,
                       gr_vector_int &ninput_items,
                       gr_vector_const_void_star &input_items,
                       gr_vector_void_star &output_items)
    {
      return 0;
    }

  } /* namespace rircsim */
} /* namespace gr */

