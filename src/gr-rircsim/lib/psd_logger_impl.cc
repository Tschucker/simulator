/* -*- c++ -*- */
/*
 * Copyright 2020 gr-rircsim author.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <volk/volk.h>
#include "psd_logger_impl.h"

namespace gr {
  namespace rircsim {

    psd_logger::sptr
    psd_logger::make(std::string node)
    {
      return gnuradio::get_initial_sptr
        (new psd_logger_impl(node));
    }


    /*
     * The private constructor
     */
    psd_logger_impl::psd_logger_impl(std::string node)
      : gr::sync_block("psd_logger",
              gr::io_signature::make(1, 1, sizeof(gr_complex)),
              gr::io_signature::make(0, 0, 0)),
              m_node(node)
    {
      char path[100];
      sprintf(path, "/tmp/%s.psd-log", node.c_str());
      m_file = fopen(path, "wb");

      m_fft_in = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * NUM_POINTS);
      m_fft_out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * NUM_POINTS);
      m_fft_plan = fftwf_plan_dft_1d(NUM_POINTS, m_fft_in, m_fft_out, FFTW_FORWARD, FFTW_ESTIMATE);
    }

    /*
     * Our virtual destructor.
     */
    psd_logger_impl::~psd_logger_impl()
    {
      fclose(m_file);
      fftwf_destroy_plan(m_fft_plan);
      fftwf_free(m_fft_in);
      fftwf_free(m_fft_out);
    }

    int
    psd_logger_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const gr_complex *in = (const gr_complex *) input_items[0];

      int offset = 0;
      while(offset + NUM_POINTS < noutput_items)
      {
        // FFT
        float scale = 1.0 / NUM_POINTS;
        for(int x = 0; x < NUM_POINTS*2; x++)
        {
          ((float *)m_fft_in)[x] = ((float *)in)[offset*2+x] * scale;
        }
        fftwf_execute(m_fft_plan);

        // PSD sum
        gr_complex dp;
        memcpy(m_fft_in, m_fft_out, 8*NUM_POINTS);
        volk_32fc_x2_conjugate_dot_prod_32fc(&dp, (lv_32fc_t *)m_fft_in, (lv_32fc_t *)m_fft_out, NUM_POINTS);
        float sum = std::abs(dp);

        // scale
        sum /= (float)NUM_POINTS;

        // convert to dB
        sum = 10 * log10(sum);
        if(isinff(sum)) sum = -100;

        fprintf(m_file, "%f\n", sum);
        offset += NUM_POINTS;
      }

      consume(0, offset);

      fflush(m_file);

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace rircsim */
} /* namespace gr */

