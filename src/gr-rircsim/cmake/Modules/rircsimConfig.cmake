INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_RIRCSIM rircsim)

FIND_PATH(
    RIRCSIM_INCLUDE_DIRS
    NAMES rircsim/api.h
    HINTS $ENV{RIRCSIM_DIR}/include
        ${PC_RIRCSIM_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    RIRCSIM_LIBRARIES
    NAMES gnuradio-rircsim
    HINTS $ENV{RIRCSIM_DIR}/lib
        ${PC_RIRCSIM_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/rircsimTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(RIRCSIM DEFAULT_MSG RIRCSIM_LIBRARIES RIRCSIM_INCLUDE_DIRS)
MARK_AS_ADVANCED(RIRCSIM_LIBRARIES RIRCSIM_INCLUDE_DIRS)
