/* -*- c++ -*- */

#define RIRCSIM_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "rircsim_swig_doc.i"

%{
#include "rircsim/liquid_flex_rx.h"
#include "rircsim/liquid_flex_tx.h"
#include "rircsim/psd_logger.h"
#include "rircsim/muxer_iq_source.h"
#include "rircsim/muxer_iq_sink.h"
#include "rircsim/muxer_synchronous.h"
%}


%include "rircsim/liquid_flex_rx.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, liquid_flex_rx);

%include "rircsim/liquid_flex_tx.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, liquid_flex_tx);
%include "rircsim/psd_logger.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, psd_logger);


%include "rircsim/muxer_iq_source.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, muxer_iq_source);
%include "rircsim/muxer_iq_sink.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, muxer_iq_sink);
%include "rircsim/muxer_synchronous.h"
GR_SWIG_BLOCK_MAGIC2(rircsim, muxer_synchronous);

