#!/usr/bin/env python3

import os
import pmt
import time
import zmq

# Blue 1 - packet input
tx_context = zmq.Context()
tx_socket = tx_context.socket(zmq.PUB)
tx_socket.bind("tcp://0.0.0.0:44001")

# Blue 2 - packet output
rx_context = zmq.Context()
rx_socket = rx_context.socket(zmq.SUB)
rx_socket.connect("tcp://blue2:44002")
rx_socket.subscribe("")

# convert a byte string to a serialized PMT blob
def bytes_to_serialized_pmt(b):
    v = pmt.init_u8vector(len(pld), pld)
    d = pmt.cons(pmt.to_pmt(len(pld)), v)
    s = pmt.serialize_str(d)
    return s

# wait for for the TX socket to be connectd

time.sleep(1)

# tx_socket.recv()
rx_count = 0
packet_count = 100
for x in range(packet_count):

    print("sending packet %d of %d" % (x, packet_count))

    pld = b"%d" % x
    pld = b"-".join(p for p in [pld]*50)
    
    b = bytes_to_serialized_pmt(pld)
    print(tx_socket.send(b))

    if rx_socket.poll(timeout=0):
        rx_socket.recv()
        rx_count += 1

start = time.time()
while True:

    elapsed = time.time() - start
    if elapsed >= 5 or rx_count == packet_count:
        break

    if rx_socket.poll(timeout=0):
        rx_socket.recv()
        rx_count += 1
        print(rx_count)


print("Sent %d packets, received %d." % (packet_count, rx_count))